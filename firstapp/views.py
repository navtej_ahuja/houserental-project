from django.shortcuts import render
from django.http import HttpResponse
import requests

def home(request):
    return render(request,"home.html")

def signin(request):
    return render(request,"signin.html")

def signup(request):
    return render(request,"signup.html")

def aboutus(request):
    return render(request,"aboutus.html")

def contact(request):
    return render(request,"contact.html")

def properties(request):
    return render(request,"property.html")
